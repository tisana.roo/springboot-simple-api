package me.tisana.simpleapi.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SimpleResource {
    @GetMapping("/hello")
    public String helloWorld(){
        return "Hellow World";
    }
}
